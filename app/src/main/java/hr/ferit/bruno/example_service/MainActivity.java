package hr.ferit.bruno.example_service;

import android.app.Service;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.bStartService) Button bStartService;
    @BindView(R.id.bStopService) Button bStopService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.bStartService,R.id.bStopService})
    public void onClick(Button button){
        switch (button.getId()){
            case R.id.bStartService:
                startLocationService(); break;
            case R.id.bStopService:
                stopLocationService(); break;
        }
    }

    private void startLocationService() {
        Intent serviceStartingIntent = new Intent(this, LocationTrackingService.class);
        this.startService(serviceStartingIntent);
    }

    private void stopLocationService() {
        Intent serviceStoppingIntent = new Intent(this, LocationTrackingService.class);
        this.stopService(serviceStoppingIntent);
    }

}
