package hr.ferit.bruno.example_service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by Zoric on 13.10.2017..
 */

public class LocationTrackingService extends Service {

    private static final String TAG = "LOCATION_SERVICE";
    private static final int MIN_TIME = 5000;
    private static final int MIN_DISTANCE = 10;
    private LocationManager mLocationManager;
    private LocationListener mLocationListener;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.d(TAG, "start command recieved");
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.mLocationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        this.mLocationListener = new SimpleLocationListener();
        try {
            this.mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,MIN_TIME, MIN_DISTANCE,this.mLocationListener);
        }catch (SecurityException e){e.printStackTrace();}
        Log.d(TAG, "in onCreate");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            this.mLocationManager.removeUpdates(this.mLocationListener);
        }catch (Exception exception){ exception.printStackTrace(); }
        Log.d(TAG, "in onDestroy");
    }

    private class SimpleLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            Log.d(TAG, location.toString());
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    }
}
